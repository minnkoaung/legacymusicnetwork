var data = [
	{
		id: 0,
		name: 'Opening Ceremony',
		org: 'DevCon Yangon',
		position: 'Working Group',
		topic: 'Opening Ceremony',
		summary: '',
		day: 1,
		room: 'Conference Room',
		time: '8:30 AM - 9:45 AM',
		favorite: false,
		photo: true
	},
	{
		id: 1,
		name: 'U Win Htein Win',
		org: 'Guesto Institute',
		position: 'Founder & Principal',
		topic: 'Keynote Speech',
		summary: '',
		day: 1,
		room: 'Conference Room',
		time: '9:45 AM - 10:00 AM',
		favorite: false,
		photo: true
	},
	{
		id: 2,
		name: 'Phone Pyae Oo',
		org: 'Organization',
		position: 'Unknown',
		topic: 'Technopreneurs, not just Developers',
		summary: '',
		day: 1,
		room: 'Conference Room',
		time: '10:00 AM - 11:00 AM',
		favorite: false,
		photo: true
	},
	{
		id: 3,
		name: 'Ko Ko Ye',
		org: 'Ubuntu Myanmar',
		position: 'LoCo Team',
		topic: 'Business for Customized OS',
		summary: '',
		day: 1,
		room: 'Conference Room',
		time: '11:00 AM - 12:00 PM',
		favorite: false,
		photo: true
	},
	{
		id: 4,
		name: 'Tin Maung Maung Yin',
		org: 'Organization',
		position: 'Unknown',
		topic: 'Building Better Cross-Platform Mobile Apps Faster',
		summary: 'This topic will discuss and present about how to develop cross-platform mobile apps (native or web-based) faster with effective and efficient development environment.',
		day: 1,
		room: 'Conference Room',
		time: '1:00 PM - 2:00 PM',
		favorite: false,
		photo: true
	}
];